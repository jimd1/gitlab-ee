export default {
  themeName: 'gitlab',
  monacoTheme: {
    base: 'vs',
    inherit: true,
    rules: [],
    colors: {
      'editorLineNumber.foreground': '#CCCCCC',
    },
  },
};
